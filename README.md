# hf_tti

#### Table of Contents
- [Introduction](#introduction)
- [Disclaimer](#disclaimer)
- [Getting started](#getting-started)
- [How to get and use Hugging Face Api Key](#how-to-get-and-use-hugging-face-api-key)
- [Credits](#credits)

## Introduction
AI: Text to Image client for terminal written in [Go](https://go.dev/).

It use various models hosted on [Hugging Face](https://huggingface.co/) hub.

To list all supported models use `--list-models` option:
```text
runwayml/stable-diffusion-v1-5 (default)
stabilityai/stable-diffusion-2-1 (high quality)
prompthero/openjourney-v4 (something different with interesting results)
dallinmackay/Van-Gogh-diffusion (Van Gogh style)
```

## Disclaimer
I'm not a professional developer. I enjoy to develop this silly utilities just for my personal use.
So, use them at your own risk.

## Getting started

#### Binary
You can download prebuilt binary packages from [here](https://gitlab.com/ai-gimlab/hf_tti/-/releases).

#### Compile from source
If you prefer, clone this repo and compile sources.

Prerequisite: [Go Development Environment](https://go.dev/dl/) installed.

Clone this repo and build:
```bash
git clone https://gitlab.com/ai-gimlab/hf_tti.git
cd hf_tti
go mod init hf_tti && go mod tidy
go build .
```

#### Usage
Use '--help' to view all options:
```bash
hf_tti --help
```

Output:
```txt
Usage: HF_TTI [OPTIONS] "PROMPT"

Terminal Text to Image client Hugging Face Diffusers Models

Defaults:
  model: runwayml/stable-diffusion-v1-5
  output file: output.jpeg

Order matter:
  first OPTIONS
  then PROMPT

Note: PROMPT must always be enclosed in single or double quotes

OPTIONS:
  -f, --file=FILE         file with Hugging Face api key (HF_API_KEY=apikey)
  -l, --list-models       list all available models
  -m, --model-name=MODEL  model name
                          use '--list-models' to list all available models
  -o, --out-file=OUTFILE  where to output resulting image file
      --help              print this help
      --version           output version information and exit

  💡                      HF_TTI --model-name "prompthero/openjourney-v4" -o images/myFile.jpeg "Horses running over a galaxy"
```

## How to get and use Hugging Face Api Key

Get your api key from [Hugging Face](https://huggingface.co/join) site.

There are three way to supply api key:

#### 1) default ####
Create file `.env` and insert the following line:
```bash
HF_API_KEY='hf-YOUR-API-KEY'
```
Copy file `.env` to `$HOME/.local/etc/` folder.

#### 2) environment variable ####
If you prefer, export HF_API_KEY as environment variable:
```bash
export HF_API_KEY='hf-YOUR-API-KEY'
```

#### 3) use '--file' option ####
You can also supply your own key file, containing the statement `HF_API_KEY='hf-YOUR-API-KEY'`, and pass its path as argument to '--file' option.

## Credits
This project is made possible thanks to the use of the following libraries and the precious work of those who create and maintain them.
Of course thanks also to all those who create and maintain the AI models.

- [godotenv](https://github.com/joho/godotenv)
- [getopt](https://github.com/pborman/getopt/)
- [hfapigo](https://github.com/TannerKvarfordt/hfapigo)
- [models](https://huggingface.co/models?pipeline_tag=text-to-image&sort=trending)
- [Hugging Face Hub](https://huggingface.co/)

---

[Others gimlab repos](https://gitlab.com/users/gimaldi/groups)

---
