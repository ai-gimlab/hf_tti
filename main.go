package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/sysUtils/go-libraries/libhelp"
	"gitlab.com/sysUtils/go-libraries/libopt"

	"github.com/pborman/getopt/v2"
)

func main() {
	var (
		logger *log.Logger = log.New(os.Stderr, logPrefix, log.Lshortfile)
	)

	// --- check user input chars
	// command name
	if err := libopt.ChkChars(chrLowCase+chrUpCase+chrNums+chrCmdName, os.Args[:1]); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}

	// --- check cmdline options
	err := getopt.Getopt(nil)
	switch {
	case flagHelp:
		libhelp.PrintHelp(helpMsgUsage, helpMsgBody, helpMsgTips, true)
		os.Exit(0)
	case flagVersion:
		libhelp.PrintVersion(verMsg)
		os.Exit(0)
	case err != nil:
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	case flagListModels:
		fmt.Printf("%s\n", listMsg)
		os.Exit(0)
	}

	// check options with string argument for "-" char as first char of argument
	if strings.HasPrefix(flagFile, "-") {
		logger.Fatal(fmt.Sprintf("\a%s: '%s'\n%s", "wrong parameter for -f or --file", flagFile, tryMsg))
	}
	if strings.HasPrefix(flagOutputFile, "-") {
		logger.Fatal(fmt.Sprintf("\a%s: '%s'\n%s", "wrong parameter for -o or --out-file", flagOutputFile, tryMsg))
	}
	if strings.HasPrefix(flagModel, "-") {
		logger.Fatal(fmt.Sprintf("\a%s: '%s'\n%s", "wrong parameter for -m or --model-name", flagModel, tryMsg))
	}

	// --- check non-options arguments: prompt message
	nonOptArgs := getopt.Args()
	if len(nonOptArgs) == 0 {
		logger.Fatal(fmt.Errorf("\a%s\n%s", "missing PROMPT", tryMsg))
	}
	prompt = nonOptArgs[0]

	// --- analyze flags
	if flagModel != "" {
		model = flagModel
	}
	if flagOutputFile != "" {
		outputFile = flagOutputFile
	}

	// send prompt to model
	if err := httpRequest(prompt); err != nil {
		logger.Fatal(fmt.Errorf("\a%s\n%s", err, tryMsg))
	}
}
