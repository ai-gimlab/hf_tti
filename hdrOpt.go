package main

import (
	"github.com/pborman/getopt/v2"
)

// Declare flags and have getopt return pointers to the values.
var (
	flagHelp       bool
	flagVersion    bool
	flagFile       string
	flagListModels bool
	flagModel      string
	flagOutputFile string
)

// init vars at program start with 'init()' internal function
func init() {
	getopt.FlagLong(&flagHelp, "help", '\U0001f595', "")
	getopt.FlagLong(&flagVersion, "version", '\U0001f6bd', "")
	getopt.FlagLong(&flagFile, "file", 'f', "")
	getopt.FlagLong(&flagListModels, "list-models", 'l', "")
	getopt.FlagLong(&flagModel, "model-name", 'm', "")
	getopt.FlagLong(&flagOutputFile, "out-file", 'o', "")
}
