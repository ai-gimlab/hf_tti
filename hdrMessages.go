package main

const helpMsgUsage = "Usage: %s [OPTIONS] \"PROMPT\"\n"

const helpMsgBody = `
Terminal Text to Image client Hugging Face Diffusers Models

Defaults:
  model: stabilityai/stable-diffusion-2-1
  output file: output.jpeg

Order matter:
  first OPTIONS
  then PROMPT

Note: PROMPT must always be enclosed in single or double quotes

OPTIONS:
  -f, --file=FILE         file with Hugging Face api key (HF_API_KEY=apikey)
  -l, --list-models       list all available models
  -m, --model-name=MODEL  model name
                          use '--list-models' to list all available models
  -o, --out-file=OUTFILE  where to output resulting image file
      --help              print this help
      --version           output version information and exit
`

const helpMsgTips = "  💡                      %s --model-name \"prompthero/openjourney-v4\" -o images/myFile.jpeg \"Horses running over a galaxy\"\n"

// befor this var, always print cmd name, without newline char (os.Args[0]). eg: fmt.Fprintf(os.Stdout, "%s", os.Args[0]); fmt.Println(verMsg)
const verMsg = ` (gimlab) 0.3.5
Copyright (C) 2023 gimlab.
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.`

const listMsg = `models:
  stabilityai/stable-diffusion-2-1	(default)
  prompthero/openjourney-v4		(something different with interesting results)
  dallinmackay/Van-Gogh-diffusion	(Van Gogh style)`
