package main

import (
	"time"

	"gitlab.com/sysUtils/go-libraries/libhelp"
	"gitlab.com/sysUtils/go-libraries/libopt"
)

const (
	// .env file default location
	cfgRootDir string = "HOME" // used as environ var
	cfgDir     string = "/.local/etc/"
	cfgFile    string = ".env"

	// lib for permitted cmdline chars
	chrLowCase  string = libopt.ChrLowCase  // a, b, c, d, ...
	chrUpCase   string = libopt.ChrUpCase   // A, B, C, D, ...
	chrNums     string = libopt.ChrNums     // 0, 1, 2, 3, ...
	chrNumsPfix string = libopt.ChrNumsPfix // hexadecimal and binary number prefix (eg: 0x or 0b)
	chrFlags    string = libopt.ChrFlags    // used in cmdline options (eg: -a -b -c) - NOTE: include space
	chrCmdName  string = libopt.ChrCmdName  // used for command name (eg: ./mycmd)
	chrExtra    string = libopt.ChrExtra    // for special cases (eg: email.name@email.address)
	chrPath     string = libopt.ChrPath

	// API config defaults
	env               string = "HF_API_KEY"
	defaultModel      string = "stabilityai/stable-diffusion-2-1"
	defaultOutputFile string = "output.jpeg"

	// network settings
	timeout time.Duration = time.Second * 15 // network timeout
)

var (
	// vars from custom libraries
	cmdName   string = libhelp.CmdName
	tryMsg    string = libhelp.TryMsg
	logPrefix string = libhelp.LogPrefix

	// API config vars
	model      string = defaultModel
	outputFile string = defaultOutputFile
	prompt     string
	envFile    string
)
