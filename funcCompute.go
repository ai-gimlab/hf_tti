package main

import (
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"mime"
	"os"
	"time"

	"github.com/TannerKvarfordt/hfapigo"
	"github.com/joho/godotenv"
)

func httpRequest(input string) error {
	// function name to return if any error occur
	var funcName string = "httpRequest"

	// load env configs
	key, err := loadEnv()
	if err != nil {
		return err
	}
	hfapigo.SetAPIKey(key)

	type ChanRv struct {
		resp   image.Image
		format string
		err    error
	}
	ch := make(chan ChanRv)

	fmt.Print("Sending request")
	go func() {
		img, fmt, err := hfapigo.SendTextToImageRequest(model, &hfapigo.TextToImageRequest{
			Inputs:  input,
			Options: *hfapigo.NewOptions().SetWaitForModel(true),
		})
		ch <- ChanRv{img, fmt, err}
	}()

	for {
		select {
		default:
			fmt.Print(".")
			time.Sleep(time.Millisecond * 500)
		case chrv := <-ch:
			if chrv.err != nil {
				return fmt.Errorf("func %q - error from Hugging Face: %s", funcName, chrv.err)
			}

			fout, err := os.Create(outputFile)
			if err != nil {
				return fmt.Errorf("func %q - error: %s", funcName, err)
			}
			defer fout.Close()

			mimetype := mime.TypeByExtension(fmt.Sprintf(".%s", chrv.format))

			switch mimetype {
			case "image/jpeg":
				err = jpeg.Encode(fout, chrv.resp, nil)
			case "image/png":
				err = png.Encode(fout, chrv.resp)
			default:
				err = fmt.Errorf("unknown image format: %s", chrv.format)
			}

			if err != nil {
				return fmt.Errorf("func %q - error: %s", funcName, err)
			} else {
				fmt.Printf("\nWrote image to %s\n", outputFile)
			}

			return nil
		}
	}
}

func loadEnv() (key string, err error) {
	// function name to return if any error occur
	var funcName string = "loadEnv"

	// get env file
	if flagFile != "" {
		err := godotenv.Load(flagFile)
		if err != nil {
			return "", fmt.Errorf("func %q - error loading env file %s", funcName, flagFile)
		}
	} else {
		// from os environment
		envVar := os.Getenv(env)

		// from default .env file
		cfgEnv := os.Getenv(cfgRootDir) + cfgDir + cfgFile
		errFile := godotenv.Load(cfgEnv)

		// check result
		if errFile != nil && envVar == "" {
			return "", fmt.Errorf("func %q - error loading default env file or missing env var %q", funcName, env)
		}
	}

	// load env configs
	key, ok := os.LookupEnv(env)
	if !ok || key == "" {
		return "", fmt.Errorf("func %q - Hugging Face API KEY not given", funcName)
	}

	return key, nil
}
